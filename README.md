# jenkins docker in docker (dind)

Esse repo tem como objetivo rodar o jenkins via docker-compose e ter o docker habilitado dentro do jenkins.

Isso funciona usando o docker-in-docker, passando via volume o sock do docker da maquina hospedeira (/var/run/docker.sock) para o container do jenkins.

CUIDADO: não faça isso em produção! o jenkins está acessando o docker da maquina hospedeira e se o jenkins for comprometido pode comprometer a maquina hospedeira.

## Como usar

Como pré-requisito, tenha instalado o docker e o docker-compose.

Basta subir o `docker-compose.yml`: `docker-compose up -d`

E abrir o navegador em [localhost:8080](http://localhost:8080/)
