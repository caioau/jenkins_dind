FROM jenkins/jenkins:lts

USER root

# instala o docker e add o user jenkins ao grupo do docker
RUN apt-get update && \
    apt-get -y --no-install-recommends install curl sudo && \
    curl -fsSL https://get.docker.com | sh - && \
    usermod -aG docker jenkins && \
    apt clean && \
    apt autoclean && \
    rm -rf /var/lib/apt/lists/* && \
    groupmod -o -g 998 docker

COPY entrypoint_patched.sh /usr/local/bin/jenkins.sh
